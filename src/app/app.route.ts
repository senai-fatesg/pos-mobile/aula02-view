import {Routes} from '@angular/router'
import { HomeComponent } from './home/home.component'
import { ProdutoComponent } from './produto/produto.component'
import { ClienteComponent } from './cliente/cliente.component'
import { LoginComponent } from './login/login.component'
import { AuthGuard } from './auth.guard'

export const ROUTES: Routes = [
    {path:'', component: HomeComponent, canActivate: [AuthGuard]},
    {path:'produto', component: ProdutoComponent, canActivate: [AuthGuard]},
    {path:'cliente', component: ClienteComponent, canActivate: [AuthGuard]},
    {path:'login', component: LoginComponent},
]